#!/bin/bash

## Stop existing stack if running
docker-compose down

## Create ACME file
touch acme.json
chmod 600 acme.json

## Create network
docker network create frontend

## Set permissions on app directory (33 = www-data id for nginx)
mkdir -p app
chown 33:33 -R ./app
find ./app -type d -exec chmod 750 {} \;
find ./app -type f -exec chmod 640 {} \;

## Now start everything
docker-compose up -d --force-recreate

## Attach to the log output so we can see things working
docker-compose logs -f