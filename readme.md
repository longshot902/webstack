Web Stack
=========


*NOTE* This is a quick and dirty webstack (with some hardening). I use this template for deploying quick web based applications for projects. 


Overivew
--------
Deploys a quick HTTPS webstack with TRAEFIK + LETS ENCRYPT + NGINX + PHP-FPM

The `start.sh` does a couple things.

1. Stops an existing deployment if running. (Start is probably a bad name, but whatever...)
2. Ensures an `acme.json` file is available for our traefik container to save our Let's Encrypt certs.
3. Makes sure docker has the appropriate networks available.
4. Stands up the compose infrastructure.
5. Builds an `app` directory and sets file permissions. (This is where your PHP app will live)


Let's Encrypt Gotcha's
----------------------
The following ports will need to open to the outside word and DNS already in place.

* 80/TCP -> Used by Traefik to capture HTTP traffic and upgrade to HTTPS
* 443/TCP -> Used by Traefik to serve our main container from NGINX + PHP-FPM
* 8080/TCP -> (Not used in this stake) Only needed if you want a dashboard available
